<?php
use yii\helpers\Url;

?>
<!--main content start-->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <article class="post">
                    <div class="post-thumb">
                        <img src="<?= $article->getImage(); ?>" alt="">
                    </div>
                    <div class="post-content">
                        <header class="entry-header text-center text-uppercase">
                            <h6><a href="<?= Url::toRoute(['site/category', 'id'=>$article->category->id]) ?>"> <?= $article->category->title; ?></a></h6>

                            <h1 class="entry-title"><a href="<?= Url::toRoute(['site/view', 'id'=>$article->id]) ?>"><?= $article->title; ?></a></h1>


                        </header>
                        <div class="entry-content">
                            <?= $article->content ?>
                        </div>

<!--                        <div class="decoration">-->
<!--                            <a href="#" class="btn btn-default">Decoration</a>-->
<!--                            <a href="#" class="btn btn-default">Decoration</a>-->
<!--                        </div>-->

                        <div class="social-share">
							<span class="social-share-title pull-left text-capitalize">By Rubel On <?= $article->getDate(); ?></span>
                            <ul style="display: flex; align-items: center" class="text-center pull-right">
                                <li style="margin-right: 20px;"><script type="text/javascript">
                                    document.write(VK.Share.button({title: 'Заголовок страницы'}, {type: 'custom', text: '<img src="http://vk.com/images/vk32.png" />'}));
                                </script></li>

                                <div id="fb-root"></div>
                                <script>(function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.0';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));</script>

                                <li><div class="fb-share-button" data-layout="button" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fvk.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Поделиться</a></div></li>
                            </ul>
                        </div>
                    </div>


                    <div id="hypercomments_widget"></div>
                    <script type="text/javascript">
                        var _hcp = _hcp || {};
                        _hcp.widget_id = 104295;
                        _hcp.widget = "Stream";
                        _hcp.xid = "<?=($article->id)?>";
                        (function() {
                            var hcc = document.createElement("script");
                            hcc.type = "text/javascript"; hcc.async = true;
                            hcc.src = ("https:" == document.location.protocol ? "https" : "http")
                                +"://widget.hypercomments.com/apps/js/hc.js";
                            var s = document.getElementsByTagName("script")[0];
                            s.parentNode.insertBefore(hcc, s.nextSibling);
                        })();
                    </script>




                </article>
            </div>
            <?= $this->render('../partials/sidebar', [
                'popular'=>$popular,
                'recent'=>$recent,
                'categories'=>$categories,
            ]);?>
        </div>
    </div>
</div>
<!-- end main content-->