<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\ImageUpload;
use app\models\Tag;
use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title'); //массив тегов из базы
        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'title'); //массив категорий из базы

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // если статья сохранена в базе (поля: заголовок, описание, контент и дата,
            // присвоен id новой статьи в базе, приступаем теперь к сохранению тегов и категории в базу.

            $tags = Yii::$app->request->post('tags'); // массив выбранных тегов пользователем в форме
            $model->saveTags($tags); // сохраняем в таблицу article_tag выбранные теги


            $category = Yii::$app->request->post('category'); // выбранная категория пользователем в форме
            $model->saveCategory($category); // привязываем category_id к статье

            $file = UploadedFile::getInstance($model, 'image'); //получили картинку (объект)
            if ($file)
            {
                $imageUpload = new ImageUpload();
                $filename = $imageUpload->uploadFile($file, null); //загрузка картинки и в $filename = "название"
                $model->saveImage($filename); // сохраняем это название
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('_form', [
            'model' => $model,
            'tags' => $tags,
            'categories' => $categories
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title'); //массив тегов из базы
        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'title'); //массив категорий из базы

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('_form', [
            'model' => $model,
            'tags' => $tags,
            'categories' => $categories
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSetImage($id)
    {
        $model = new ImageUpload();


        if (Yii::$app->request->isPost){

            $article = $this->findModel($id);
            $file = UploadedFile::getInstance($model, 'image');


            if($article->saveImage($model->uploadFile($file, $article->image)))
            {
                return $this->redirect(['view', 'id'=>$article->id]);
            }

        }


        return $this -> render('image', ['model' => $model]);
    }

    public function actionSetCategory($id)
    {
        $article = $this->findModel($id);
        $selectedCategory = ($article->category) ?  $article->category->id : 0; // НЕ РАБОТАЕТ ПРИСВОЕНИЕ НУЛЮ
        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'title');

        if(Yii::$app->request->isPost)
        {
            $category = Yii::$app->request->post('category');
            if($article->saveCategory($category))
            {
                return $this->redirect(['view', 'id'=>$article->id]);
            }
        }



        return $this->render('category', [
            'article'=>$article,
            'selectedCategory'=>$selectedCategory,
            'categories'=>$categories
        ]);
    }

    public function actionSetTags($id)
    {
        $article = $this -> findModel($id);
        $selectedTags = $article->getSelectedTags();
        $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title');

        if (Yii::$app->request->isPost)
        {
            $tags = Yii::$app->request->post('tags');
            $article->saveTags($tags);
            return $this->redirect(['view', 'id'=>$article->id]);
        }


        return $this->render('tags', [
            'selectedTags' => $selectedTags,
            'tags'=>$tags
        ]);
    }
}
