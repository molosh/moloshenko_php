<?php

namespace app\models;
use yii\base\Model;

class SignupForm extends Model
{
    public $name;
    public $email;
    public $password;
    public $confirm_password;

    public function rules()
    {
        return [
            [['name', 'email', 'password', 'confirm_password'], 'required'],

            [['name'], 'string'],
            [['email'], 'email'],

            [['name'], 'unique', 'targetClass'=>'app\models\User', 'targetAttribute'=> 'name', 'message'=>'Это имя уже занято' ],
            [['email'], 'unique', 'targetClass'=>'app\models\User', 'targetAttribute'=> 'email' ],

            [['password', 'confirm_password'], 'string', 'min' => 6],

            [
                'confirm_password',
                'compare',
                'compareAttribute' => 'password',
                'skipOnEmpty' => false,
                'message' => 'Пароли не совпадают'
            ],
        ];
    }


    public function signup()
    {
        if($this->validate())
        {
            $user = new User();
//            foreach ($this->attributes as $key=>$attribute) {
//                $user->setAttribute($key,$attribute);
//                $user->setAttributes()
//            }
            $user->setAttributes($this->attributes);

            $user->password = \Yii::$app->getSecurity()->generatePasswordHash($this->attributes['password']);
            return $user->create();
        }
    }
}