<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\web\Controller;

class AuthController extends Controller
{
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }




    public function actionSignup()
    {
        $model = new SignupForm();

        if(Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if($model->signup())
            {
                $email = $model->email;
                $this->signupEmail($email);
                return $this->redirect(['auth/login']);
            }
        }

        return $this->render('signup', ['model'=>$model]);
    }

    public function generateToken()
    {
        return bin2hex(random_bytes(78));
    }

    public function signupEmail($email)
    {
        $token = $this->generateToken();
        $user = User::find()->where(['email'=>$email])->one();
        $user->email_confirm_token = $token;
        $user->save();
        $this->testMailer($token, $email);
        Yii::$app->session->setFlash('alerts', "We have sent you a confirmation email on $email. Please, " .
            "confirm it and then login");

    }

    public function testMailer($token, $email) {
        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/site/confirm/$token";
        Yii::$app->mailer->compose()
            ->setFrom('test-dunice@yandex.ru')
            ->setTo($email)
            ->setSubject('confirmation')
            ->setTextBody($url)
            ->setHtmlBody("<b>$url</b>")
            ->send();
    }

    public function actionTest()
    {
        $user = User::findOne(1);

        Yii::$app->user->login($user);

        if(Yii::$app->user->isGuest){
            echo 'guest';
        }else{
            echo 'user';
        }
    }
}
