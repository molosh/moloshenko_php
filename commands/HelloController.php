<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Category;
use app\models\Tag;
use app\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionHaha()
    {
        // добавим админа в таблицу user
        $user = new User();
        $user->name = "admin";
        $user->password = Yii::$app->getSecurity()->generatePasswordHash('du1ni2ce3');
        $user->email = "dunice.test.blog@yandex.ru";
        $user->isAdmin = 1;
        $user->status = 1;
        $user->save();

        // добавим 3 категори в таблицу category
        $model = new Category();
        $model->id = 1;
        $model->title = 'Автомобили';
        $model->save();

        $model = new Category();
        $model->id = 2;
        $model->title = 'Музыка';
        $model->save();

        $model = new Category();
        $model->id = 3;
        $model->title = 'Коты';
        $model->save();

        // добавим 5 тегов в таблицу tags
        $model = new Tag();
        $model->id = 1;
        $model->title = 'Тег 1';
        $model->save();

        $model = new Tag();
        $model->id = 2;
        $model->title = 'Тег 2';
        $model->save();

        $model = new Tag();
        $model->id = 3;
        $model->title = 'Тег 3';
        $model->save();

        $model = new Tag();
        $model->id = 4;
        $model->title = 'Тег 4';
        $model->save();

        $model = new Tag();
        $model->id = 5;
        $model->title = 'Тег 5';
        $model->save();

    }
}
